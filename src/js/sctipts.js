"use strict";

class Card {
    constructor(id, name, username, email, title, body) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.title = title;
        this.body = body;
        this.container = document.createElement('div');
    }    

    createElements() {
        this.container.className = 'news__posts-article';

        this.container.innerHTML = `
                <img src="https://i.pravatar.cc/70" alt="Profile Image" class="news__post-profile-image">

                <div class="news__flex-container">
                    <div class="news__flex-items-container">
                        <div class="news__post-fullname">${this.name}<span>@${this.username}</span></div>
                        <button class="news__delete-post-btn">
                            <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 48 48" width="20px" height="20px"><path fill="#f4212e" d="M 24 4 C 20.704135 4 18 6.7041348 18 10 L 11.746094 10 A 1.50015 1.50015 0 0 0 11.476562 9.9785156 A 1.50015 1.50015 0 0 0 11.259766 10 L 7.5 10 A 1.50015 1.50015 0 1 0 7.5 13 L 10 13 L 10 38.5 C 10 41.519774 12.480226 44 15.5 44 L 32.5 44 C 35.519774 44 38 41.519774 38 38.5 L 38 13 L 40.5 13 A 1.50015 1.50015 0 1 0 40.5 10 L 36.746094 10 A 1.50015 1.50015 0 0 0 36.259766 10 L 30 10 C 30 6.7041348 27.295865 4 24 4 z M 24 7 C 25.674135 7 27 8.3258652 27 10 L 21 10 C 21 8.3258652 22.325865 7 24 7 z M 13 13 L 35 13 L 35 38.5 C 35 39.898226 33.898226 41 32.5 41 L 15.5 41 C 14.101774 41 13 39.898226 13 38.5 L 13 13 z M 20.476562 17.978516 A 1.50015 1.50015 0 0 0 19 19.5 L 19 34.5 A 1.50015 1.50015 0 1 0 22 34.5 L 22 19.5 A 1.50015 1.50015 0 0 0 20.476562 17.978516 z M 27.476562 17.978516 A 1.50015 1.50015 0 0 0 26 19.5 L 26 34.5 A 1.50015 1.50015 0 1 0 29 34.5 L 29 19.5 A 1.50015 1.50015 0 0 0 27.476562 17.978516 z"/></svg>
                        </button>
                    </div>

                    <div class="news__content-container">
                        <h2 class="news__post-title">${this.title}</h2>
                        <p class="news__post-content">${this.body}</p>
                        <div class="news__post-user-email">${this.email}</div>
                    </div>
                </div>
        `;

        

    }

    deletePost () {
        this.deleteButton = document.querySelector('.news__delete-post-btn');
    
        this.deleteButton.addEventListener("click", ev => {
            axios.delete(`${cardURL}/${this.id}`,  {
            //   headers: {
            //     Authorization : `Bearer ${localStorage.getItem(TOKEN)}`
            //   }

          })
          .then(({status}) => {
            if(status===200) {
              this.container.remove();
            }
          });
        });
    }

    render() {
        this.createElements();
        document.querySelector('.news__posts').prepend(this.container);
        
    }

}

const cardURL = `https://ajax.test-danit.com/api/json/posts`;

// axios.get('https://ajax.test-danit.com/api/json/users')
//     .then(({
//         data
//     }) => console.log(data))
//     .catch((err) => console.log(err));

// axios.get('https://ajax.test-danit.com/api/json/posts')
//     .then(({
//         data
//     }) => console.log(data))
//     .catch((err) => console.log(err));

new Card(1, 'jhklk', 'weokfwkef', 'ksjdnfks@kdsk.com', "skjdkflsdf", "jsndfkjwnplkdfl;lsd;fl,soekflwkmeflwkmjknkdfjeorifjeowiefowikfowoefwoiefjwfkwjenfjwjfenkwe").render();
